package de.walamana.lateinon.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import de.walamana.lateinon.R
import de.walamana.lateinon.SearchResult
import kotlinx.android.synthetic.main.item_result.view.*
import java.util.*

/**
 * Created by jonas on 26.03.2018.
 */

class ResultsAdapter : RecyclerView.Adapter<ResultsAdapter.ViewHolder>() {

    var items: ArrayList<SearchResult> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder? {
        return ResultViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_result, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var resultHolder = holder as ResultViewHolder
        resultHolder.cause.text = items[position].cause
        resultHolder.causeName.text = items[position].causeName
        resultHolder.mainFormFirst.text = items[position].mainFormFirst
        resultHolder.mainForm.text = items[position].mainForm
        resultHolder.type.text = items[position].type
        resultHolder.typeName.text = items[position].typeName
        resultHolder.translation.text = items[position].translation
    }

    override fun getItemCount(): Int {
        return items.size;
    }

    override fun getItemViewType(position: Int): Int {
        return 0;
    }

    public fun addResult(result: SearchResult){ items.add(result); this.notifyDataSetChanged() }
    public fun clearItems(){ items.clear(); this.notifyDataSetChanged() }


    open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class ResultViewHolder(itemView: View) : ResultsAdapter.ViewHolder(itemView){

        public var cause: TextView = itemView.findViewById(R.id.item_cause)
        public var causeName: TextView = itemView.findViewById(R.id.item_cause_label)
        public var mainFormFirst: TextView = itemView.findViewById(R.id.item_main_form_first)
        public var mainForm: TextView = itemView.findViewById(R.id.item_main_form)
        public var type: TextView = itemView.findViewById(R.id.item_type)
        public var typeName: TextView = itemView.findViewById(R.id.item_type_label)
        public var translation: TextView = itemView.findViewById(R.id.item_translation);

    }
}
