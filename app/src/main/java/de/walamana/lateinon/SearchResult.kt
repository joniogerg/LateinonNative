package de.walamana.lateinon

/**
 * Created by jonas on 26.03.2018.
 */

// public class SearchResult(cause: String, mainFormFirm: String, mainForm: String, translation: String)

public data class SearchResult(public val cause: String,
                               public val causeName: String,
                               public val type: String,
                               public val typeName: String,
                               public val mainFormFirst: String,
                               public val mainForm: String,
                               public val translation: String)
