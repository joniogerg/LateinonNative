package de.walamana.lateinon


import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import de.walamana.lateinon.adapters.ResultsAdapter
import kotlinx.android.synthetic.main.fragment_vocab.*
import org.jsoup.Jsoup
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import javax.security.auth.callback.Callback


/**
 * A simple [Fragment] subclass.
 */
class VocabFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater!!.inflate(R.layout.fragment_vocab, container, false)
        // Inflate the layout for this fragment


        var results: RecyclerView = view.findViewById(R.id.results)
        var submit: Button = view.findViewById(R.id.submit)
        var input: EditText = view.findViewById(R.id.input)

        var resultsAdapter = ResultsAdapter()

        results.adapter = resultsAdapter
        results.setHasFixedSize(true)
        results.layoutManager = LinearLayoutManager(view.context)

        submit.setOnClickListener {
            SearchTask(resultsAdapter).execute(input.text.toString())
        }

        return view
    }


    private inner class SearchTask(private val adapter: ResultsAdapter) : AsyncTask<String, Void, ArrayList<SearchResult>>(){


        public val TAG = "SearchTask"

        override fun doInBackground(vararg query: String?): ArrayList<SearchResult> {


            var results = ArrayList<SearchResult>()

            var url = URL("https://lateinon.de/dict/searchResults.php?lang=&q=" + query[0])
            var document = Jsoup.parse(url, 10000)


            Log.d(TAG, "Searching for " + query[0])
            Log.d(TAG, document.html())

            for(resultEl in document.select(".ausgabe")){


                var els = resultEl.select(".ausgabe-cell")
                Log.d(TAG, els.text());

                var cause = ""
                var causeName = ""
                var type = ""
                var typeName = ""
                var mainFormFirst = ""
                var mainForm = ""
                var translation = ""

                for(el in els){
                    Log.d(TAG, el.text());
                    if(el.select(".cell-left").text() == "Fall" || el.select(".cell-left").text() == "Form" ){
                        cause = el.select(".cell-right").text()
                        causeName = el.select(".cell-left").text()
                    }else if(el.select(".cell-left").text() == "Grundform"){
                        var select = el.select(".cell-right").text().split(",")
                        mainFormFirst = select[0]
                        for(str in select.subList(1, select.size)){
                            mainForm += ", " + str
                        }
                    }else if(el.select(".cell-left").text() == "Deklination" || el.select(".cell-left").text() == "Konjugation"){
                        typeName = el.select(".cell-left").text()
                        type = el.select(".cell-right").text()
                    }else if(el.select(".cell-left").text() == "Bedeutung"){
                        translation = el.select(".cell-right").text()
                    }
                }

                results.add(SearchResult(cause, causeName, type, typeName, mainFormFirst, mainForm, translation));

            }


            return results;
        }

        override fun onPostExecute(results: ArrayList<SearchResult>) {
            super.onPostExecute(results)

            adapter.clearItems()

            for(result in results){
                adapter.addResult(result)
            }
        }

    }

}// Required empty public constructor
